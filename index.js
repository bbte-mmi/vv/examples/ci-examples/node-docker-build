import express from 'express';

const app = express();
const port = process.env.PORT || 8080;

app.use(express.static('public'));

app.listen(port, () => {
  console.log(`App listening on http://localhost:${port}`);
});

process.on('SIGTERM', () => process.exit(1));
process.on('SIGINT', () => process.exit(1));
