# Node.js+express Docker example

Build a Node.js server and run locally.

```bash
# build image
docker build -t node-express-server .
# run container with port forwarding
docker run -it --rm -e PORT=12345 -p 12345:12345 node-express-server
```
