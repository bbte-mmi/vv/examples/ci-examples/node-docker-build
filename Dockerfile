# start with node image this time
FROM node:18.16.0-alpine

# set working directory
WORKDIR /usr/src

# copy app (ignores apply)
COPY package* ./

# install dependencies
RUN npm install

# copy app (ignores apply)
COPY . ./

# set PORT default env
ENV PORT=12345

# mark 12345 as exposed port (informative only)
EXPOSE 12345

# run node when VM starts
CMD node index.js
